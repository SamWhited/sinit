# Simple Init (sinit)

Simple Init is a simple init system written in Rust that proxies syscalls to its
child process(es).

It was mostly written as a learning exercise, but may actually be useful in the
real world too… because Linux treats PID1 as a special snowflake, when you run
your applications in a Docker container (as PID1) it's possible that if they
ever call `fork(3)` and then the parent process dies you'll end up with a zombie
container (because there's nothing to manage the child processes anymore).
Instead, you could use `sinit` as the entry point for your container and then if
the child process dies sinit will clean up for you.

## Disclaimer

More people have reached out to me about this project than expected; it should
be noted that this was a learning exercise and probably shouldn't actually be
used in the real world without lots of vetting. There are no tests, and it's
probably very buggy and unsafe.
