// Command sigtest prints the name of any OS signals it receives to STDOUT.
package main

import (
	"fmt"
	"os"
	"os/signal"
)

func main() {
	c := make(chan os.Signal)
	signal.Notify(c)

	for sig := range c {
		fmt.Printf("Got signal: %v\n", sig)
		if sig == os.Interrupt {
			os.Exit(0)
		}
	}
}
